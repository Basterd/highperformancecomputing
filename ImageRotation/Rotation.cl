﻿__kernel void image_rotate(__global int* input, float sinTheta, float cosTheta, int xMax, int yMax, __global int* output)
{
	const int2 pos1 = {get_global_id(0), get_global_id(1)};
	const int2 pos0 = {xMax/2, yMax/2};
	const int channel = get_global_id(2);
	
	int x2 = round(cosTheta * (pos1.x - pos0.x) - sinTheta * (pos1.y - pos0.y) + pos0.x);
	int y2 = round(sinTheta * (pos1.x - pos0.x) + cosTheta * (pos1.y - pos0.y) + pos0.y);

	if (x2 >= 0 && x2 < xMax && y2 >= 0 && y2 < yMax)
		output[3 * (pos1.y * xMax + pos1.x) + channel]= input[(3 * (y2 * xMax + x2)) + channel];

}