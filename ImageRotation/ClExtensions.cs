﻿using System.Collections.Generic;
using OpenCL.Net;

namespace ImageRotation
{
    public class ClExtensions
    {
        public static void ReleaseMemObjects(List<IMem> memObjects)
        {
            foreach (var memObject in memObjects)
            {
                Cl.ReleaseMemObject(memObject);
            }
        }
    }
}
