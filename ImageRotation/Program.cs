﻿using System;
using System.Drawing;
using System.IO;

namespace ImageRotation
{
    public class Program
    {
        public static void Main(string[] args)
        {
            if (args.Length != 2)
            {
                PrintUsage();
                return;
            }

            var filePath = Path.Combine(Directory.GetCurrentDirectory(), args[0]);
            if (!File.Exists(filePath))
            {
                Console.WriteLine($"File '{filePath}' does not exist.");
                return;
            }

            if (!int.TryParse(args[1], out var degrees))
            {
                Console.WriteLine($"'{args[1]}' was not recognized as a valid number.'");
                return;
            }

            using (var file = File.OpenRead(filePath))
            {
                using (var bmp = new Bitmap(file))
                {
                    Rotation.Rotate(bmp, degrees);
                }
            }
        }

        private static void PrintUsage()
        {
            Console.WriteLine("Usage: ImageRotation <file> <degrees>");
            Console.WriteLine("\t<file> The file image to rotate");
            Console.WriteLine("\t<degrees> The degrees to rotate the input image");
        }
    }
}
