﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;

namespace ImageRotation
{
    //copied from https://stackoverflow.com/questions/24701703/c-sharp-faster-alternatives-to-setpixel-and-getpixel-for-bitmaps-for-windows-f
    public class DirectBitmap : IDisposable
    {
        public Bitmap Bitmap { get; }
        public int[] Bits { get; }
        public int[][] BitsPadding { get; }
        public bool Disposed { get; private set; }
        public int Height { get; }
        public int Width { get; }

        protected GCHandle BitsHandle { get; }

        public DirectBitmap(int width, int height, bool usePadding)
        {
            Width = width;
            Height = height;
            Bits = new Int32[width * height];
            if (usePadding)
            {
                BitsPadding = new int[width][];
                for (int i = 0; i < width; i++)
                {
                    BitsPadding[i] = new int[height];
                }
            }
            BitsHandle = GCHandle.Alloc(Bits, GCHandleType.Pinned);
            Bitmap = new Bitmap(width, height, width * 4, PixelFormat.Format32bppPArgb, BitsHandle.AddrOfPinnedObject());
        }

        public void SetPixel(int x, int y, Color color)
        {
            int col = color.ToArgb();

            Bits[x + (y * Width)] = col;
        }

        public void SetPixelPadding(int x, int y, Color color)
        {
            BitsPadding[x][y] = color.ToArgb();
        }

        public void PaddingTo1D()
        {
            for (int x = 0; x < Width; x++)
            {
                for (int y = 0; y < Height; y++)
                {
                    Bits[x + (y * Width)] = BitsPadding[x][y];
                }
            }
        }

        public Color GetPixel(int x, int y)
        {
            int index = x + (y * Width);
            int col = Bits[index];
            Color result = Color.FromArgb(col);

            return result;
        }

        public void Dispose()
        {
            if (Disposed) return;
            Disposed = true;
            Bitmap.Dispose();
            BitsHandle.Free();
        }
    }
}
