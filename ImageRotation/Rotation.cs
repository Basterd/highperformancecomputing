﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using OpenCL.Net;

namespace ImageRotation
{
    internal static class Rotation
    {
        public static void Rotate(Bitmap bmp, int degrees)
        {
            var platforms = Cl.GetPlatformIDs(out var _);

            var devices = Cl.GetDeviceIDs(platforms[1], DeviceType.All, out var _);

            var ctx = Cl.CreateContext(new[] { ContextProperty.Zero }, (uint)devices.Length, devices, null,
                IntPtr.Zero, out var _);

            var kernelSource = File.ReadAllText("Rotation.cl");
            var prog = Cl.CreateProgramWithSource(ctx, 1, new[] { kernelSource }, new[] { new IntPtr(kernelSource.Length) }, out var error);

            error = Cl.BuildProgram(prog, 0, null, null, null, IntPtr.Zero);
            if (error != ErrorCode.Success)
            {
                var programInfo = Cl.GetProgramBuildInfo(prog, devices[0], ProgramBuildInfo.Log, out error);
                Console.WriteLine(programInfo.ToString());
                Console.ReadLine();
                return;
            }

            var commandQueue = Cl.CreateCommandQueue(ctx, devices[0], CommandQueueProperties.None, out error);

            var kernel = Cl.CreateKernel(prog, "image_rotate", out error);

            var imageData = GetKernelInput(bmp);

            var inputBuffer = Cl.CreateBuffer<int>(ctx, MemFlags.ReadOnly, imageData.Length * sizeof(int), out error);
            error = Cl.EnqueueWriteBuffer(commandQueue, inputBuffer, Bool.True, imageData, 0, null, out var _);
            error = Cl.SetKernelArg(kernel, 0, inputBuffer);

            error = Cl.SetKernelArg(kernel, 1, (float)Math.Sin(degrees * Math.PI / 180));
            error = Cl.SetKernelArg(kernel, 2, (float)Math.Cos(degrees * Math.PI / 180));
            error = Cl.SetKernelArg(kernel, 3, bmp.Width);
            error = Cl.SetKernelArg(kernel, 4, bmp.Height);

            var outputBuffer = Cl.CreateBuffer<int>(ctx, MemFlags.WriteOnly, imageData.Length * sizeof(int), out error);
            error = Cl.SetKernelArg(kernel, 5, outputBuffer);

            error = Cl.EnqueueNDRangeKernel(commandQueue, kernel, 3, null, new[] { new IntPtr(bmp.Width), new IntPtr(bmp.Height), new IntPtr(3) }, null, 0, null, out var _);
            
            var result = new int[imageData.Length];
            error = Cl.EnqueueReadBuffer(commandQueue, outputBuffer, Bool.True, 0, result.Length, result, 0, null, out var _);
            

            ClExtensions.ReleaseMemObjects(new List<IMem> { inputBuffer, outputBuffer});

            WriteResult(result, bmp.Width, bmp.Height, $"{Guid.NewGuid()}.bmp");
        }

        private static void WriteResult(IReadOnlyList<int> result, int width, int height, string filename)
        {
            using (var bmp = new DirectBitmap(width, height, false))
            {
                for (var x = 0; x < bmp.Width; x++)
                {
                    for (var y = 0; y < bmp.Height; y++)
                    {
                        var offset = 3 * (y * bmp.Width + x);
                        var color = Color.FromArgb(result[offset + 0], result[offset + 1], result[offset + 2]);
                        bmp.SetPixel(x, y, color);
                    }
                }

                bmp.Bitmap.Save(filename);
            }
        }

        private static int[] GetKernelInput(Bitmap bmp)
        {
            var result = new int[bmp.Width * bmp.Height * 3];
            for (var x = 0; x < bmp.Width; x++)
            {
                for (var y = 0; y < bmp.Height; y++)
                {
                    var color = bmp.GetPixel(x, y);
                    var pixelIndex = 3 * (y * bmp.Width + x);
                    result[pixelIndex + 0] = color.R;
                    result[pixelIndex + 1] = color.G;
                    result[pixelIndex + 2] = color.B;
                }
            }

            return result;
        }
    }
}