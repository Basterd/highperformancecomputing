﻿using OpenCL.Net;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Scan;

namespace StreamCompaction
{
    public class StreamCompactor
    {
        private int[] Inputs { get; }
        public long SequentialTime { get; private set; }
        public long GpuTime { get; private set; }

        public StreamCompactor(int[] inputs)
        {
            Inputs = inputs;
        }

        public int[] CompactCpu(Func<int, bool> function)
        {
            var watch = new Stopwatch();
            watch.Start();

            var result = new bool[Inputs.Length];

            Parallel.For(0, Inputs.Length, i =>
            {
                result[i] = function(Inputs[i]);
            });

            var compactedResult = new List<int>();
            for (var i = 0; i < result.Length; i++)
            {
                if (result[i])
                    compactedResult.Add(Inputs[i]);
            }

            watch.Stop();
            SequentialTime = watch.ElapsedMilliseconds;
            return compactedResult.ToArray();
        }

        public int[] CompactGpuIsFibonacci()
        {
            return CompactGpu("streamCompactionIsFibonacci");
        }

        public int[] CompactGpuIsBigger20()
        {
            return CompactGpu("streamCompactionBigger20");
        }

        public int[] CompactGpuIsPrime()
        {
            return CompactGpu("streamCompactionIsPrime");
        }

        private int[] CompactGpu(string kernelName)
        {
            var platforms = Cl.GetPlatformIDs(out var _);

            var devices = Cl.GetDeviceIDs(platforms[1], DeviceType.All, out var _);

            var ctx = Cl.CreateContext(new[] { ContextProperty.Zero }, (uint)devices.Length, devices, null,
                IntPtr.Zero, out var _);

            var kernelSource = File.ReadAllText("StreamCompaction.cl");
            var prog = Cl.CreateProgramWithSource(ctx, 1, new[] { kernelSource }, new[] { new IntPtr(kernelSource.Length) }, out var error);

            error = Cl.BuildProgram(prog, 0, null, null, null, IntPtr.Zero);
            if (error != ErrorCode.Success)
            {
                var programInfo = Cl.GetProgramBuildInfo(prog, devices[0], ProgramBuildInfo.Log, out error);
                Console.WriteLine(programInfo.ToString());
                Console.ReadLine();
                return new[] { -1 };
            }

            var commandQueue = Cl.CreateCommandQueue(ctx, devices[0], CommandQueueProperties.None, out error);
            
            //get (fake) boolean array of indizes where condition of kernel is met
            var sw = new Stopwatch();
            sw.Start();

            //int[] boolResult = ExecuteCompaction(prog, ctx, commandQueue, "streamCompactionIsPrime");

            int[] boolResult = ExecuteCompaction(prog, ctx, commandQueue, kernelName);
            sw.Stop();
            GpuTime = sw.ElapsedMilliseconds;


            //Scan bool array
            var scan = new Scan.Scan(boolResult);
            var scannedResult = scan.ScanGpu();
            GpuTime += scan.GpuTime;

            //lastTakenIndex = length of compacted array
            int lastTakenIndex = -1;
            for (int i = boolResult.Length -1; i >= 0; i--)
            {
                if (boolResult[i] == 1)
                {
                    lastTakenIndex = i;
                    break;
                }
            }

            var compactedResult = new int[scannedResult[lastTakenIndex] +1];

            sw.Start();
            compactedResult = ScatterResult(prog, ctx, commandQueue, scannedResult, boolResult, compactedResult);
            sw.Stop();
            GpuTime = sw.ElapsedMilliseconds;


            return compactedResult.ToArray();
        }

        private int[] ScatterResult(OpenCL.Net.Program prog, Context ctx, CommandQueue commandQueue, int[] scannedResult, int[] boolResult,
            int[] compactedResult)
        {
            ErrorCode error;
            var kernel = Cl.CreateKernel(prog, "scatter", out var _);

            var inputBuffer = Cl.CreateBuffer<int>(ctx, MemFlags.ReadOnly, Inputs.Length * sizeof(int), out _);
            error = Cl.EnqueueWriteBuffer(commandQueue, inputBuffer, Bool.True, Inputs, 0, null, out var _);
            error = Cl.SetKernelArg(kernel, 0, inputBuffer);

            var scannedInputBuffer = Cl.CreateBuffer<int>(ctx, MemFlags.ReadOnly, scannedResult.Length * sizeof(int), out _);
            error = Cl.EnqueueWriteBuffer(commandQueue, scannedInputBuffer, Bool.True, scannedResult, 0, null, out var _);
            error = Cl.SetKernelArg(kernel, 1, scannedInputBuffer);

            var booleanInputBuffer = Cl.CreateBuffer<int>(ctx, MemFlags.ReadOnly, boolResult.Length * sizeof(int), out _);
            error = Cl.EnqueueWriteBuffer(commandQueue, booleanInputBuffer, Bool.True, boolResult, 0, null, out var _);
            error = Cl.SetKernelArg(kernel, 2, booleanInputBuffer);

            var outputBuffer = Cl.CreateBuffer<int>(ctx, MemFlags.WriteOnly, compactedResult.Length * sizeof(int), out _);
            error = Cl.SetKernelArg(kernel, 3, outputBuffer);


            error = Cl.EnqueueNDRangeKernel(commandQueue, kernel, 1, null, new[] {new IntPtr(scannedResult.Length)}, null, 0,
                null, out var _);

            compactedResult = new int[compactedResult.Length];
            error = Cl.EnqueueReadBuffer(commandQueue, outputBuffer, Bool.True, 0, compactedResult.Length, compactedResult, 0,
                null, out var _);

            new List<IMem> { inputBuffer , outputBuffer, booleanInputBuffer, scannedInputBuffer}.ReleaseMemObjects();

            return compactedResult;
        }

        private int[] ExecuteCompaction(OpenCL.Net.Program prog, Context ctx, CommandQueue commandQueue, string kernelName)
        {
            ErrorCode error;
            var kernel = Cl.CreateKernel(prog, kernelName, out var _);

            var inputBuffer = Cl.CreateBuffer<int>(ctx, MemFlags.ReadOnly, Inputs.Length * sizeof(int), out _);
            error = Cl.EnqueueWriteBuffer(commandQueue, inputBuffer, Bool.True, Inputs, 0, null, out var _);
            error = Cl.SetKernelArg(kernel, 0, inputBuffer);

            var outputBuffer = Cl.CreateBuffer<int>(ctx, MemFlags.WriteOnly, Inputs.Length * sizeof(int), out _);
            error = Cl.SetKernelArg(kernel, 1, outputBuffer);


            error = Cl.EnqueueNDRangeKernel(commandQueue, kernel, 1, null, new[] {new IntPtr(Inputs.Length)}, null, 0, null,
                out var _);

            var boolResult = new int[Inputs.Length];
            error = Cl.EnqueueReadBuffer(commandQueue, outputBuffer, Bool.True, 0, boolResult.Length, boolResult, 0, null,
                out var _);

            new List<IMem> { inputBuffer, outputBuffer}.ReleaseMemObjects();

            return boolResult;
        }
    }
}
