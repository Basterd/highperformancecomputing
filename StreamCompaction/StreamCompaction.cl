﻿#define NUM_BANKS 16
#define LOG_NUM_BANKS 4
#define CONFLICT_FREE_OFFSET(n) \
	((n) >> NUM_BANKS + (n) >> (2 * LOG_NUM_BANKS))

int isPerfectSquare(int x)
{
    int s = (int)sqrt((double)x);
    return (s*s == x) ? 1 : 0;
}

__kernel void streamCompactionBigger20(__global int* input, __global int* output)
{
	int thid = get_global_id(0);

	if(input[thid] > 20)
		output[thid] = 1;
}

__kernel void streamCompactionIsFibonacci(__global int* input, __global int* output)
{
	int thid = get_global_id(0);

	int value = input[thid];	
	
	output[thid] =  isPerfectSquare(5*value*value + 4) ||
					isPerfectSquare(5*value*value - 4);
}


__kernel void streamCompactionIsPrime(__global int* input, __global int* output)
{
	int thid = get_global_id(0);
	int num = input[thid];

	if (num <= 1) return;
	if (num % 2 == 0 && num > 2) return;

	for(int i = 3; i < num / 2; i+= 2)
	{
		if (num % i == 0)
			return;
	}
	output[thid] = 1;
}

__kernel void scatter(__global int* input, __global int* scannedInput, __global int* booleanInput, __global int* compactedResult)
{
	int thid = get_global_id(0);

	if (booleanInput[thid] == 1)
		compactedResult[scannedInput[thid]] = input[thid];            
}