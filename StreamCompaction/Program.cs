﻿using System;
using System.Linq;

namespace StreamCompaction
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var input = RandomInput(16_500_000);
            //var input = RandomInput(16_777_216);

            var compactor = new StreamCompactor(input);

            //var cpuResult = compactor.CompactCpu(v => v > 20);
            //var gpuResult = compactor.CompactGpuIsBigger20();

            //var cpuResult = compactor.CompactCpu(IsPrime);
            //var gpuResult = compactor.CompactGpuIsPrime();

            var cpuResult = compactor.CompactCpu(IsFibonacci);
            var gpuResult = compactor.CompactGpuIsFibonacci();

            Console.WriteLine();
            Console.WriteLine($"GPU-Result is {(gpuResult.SequenceEqual(cpuResult) ? "Right" : "Wrong")}!");
            Console.WriteLine("Compared runtime:");
            Console.WriteLine($"CPU ->\t{compactor.SequentialTime}ms");
            Console.WriteLine($"GPU ->\t{compactor.GpuTime}ms");
            var factor = (double)compactor.SequentialTime / compactor.GpuTime;
            Console.WriteLine($"GPU is Factor {factor} {(factor < 1 ? "slower" : "faster")}");

            Console.ReadLine();
        }

        private static readonly Random Random = new Random();
        private static int[] RandomInput(int n)
        {
            var result = new int[n];
            for (var i = 0; i < n; i++)
            {
                result[i] = Random.Next(0, 50);
            }

            return result;
        }

        private static bool IsPrime(int num)
        {
            if (num <= 1) return false;
            if (num % 2 == 0 && num > 2) return false;

            for (int i = 3; i < num / 2; i += 2)
            {
                if (num % i == 0)
                    return false;
            }
            return true;
        }

        private static bool IsPerfectSquare(int x)
        {
            var s = (int)Math.Sqrt(x);
            return (s * s == x);
        }
        
        private static bool IsFibonacci(int n)
        {
            return IsPerfectSquare(5 * n * n + 4) ||
                   IsPerfectSquare(5 * n * n - 4);
        }
    }
}
