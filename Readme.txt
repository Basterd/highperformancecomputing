Authors:
	Peter Wurzinger - se17m035
	Manuel Juran 	- se17m010

Exercises:

ImageRotation
	This quite straightforward, we load the image, translate it into integers (intensity values), and execute our kernel, which resets the position of 	the pixels

Scan
	We implemented the Scan of the GPU Gems article.

	We only implemented the algorithm to work with (Worksize*2)^3 inputs.
	We execute the addition of the accumulated sumScan to the result on the cpu, as well as the accumulation of the sumScan itself.


	Measured avg(with 16 777 216 input elements) in ms:
		Gpu		Cpu
		410		82


StreamCompaction
	Our first Kernel checks the condition for each element, and its output is a boolean array.
	we then scan the boolean array, and receive the adresses of the inputs.
	At last we "cross" this result with the boolean array (scatter) in another kernel and retrieve our result.


	3 Examples avg in ms:
		Bigger20 (with 6.500.000 input elements)
			Gpu		Cpu
			222		110
		IsPrime (with 6.500.000 elements)
			Gpu		Cpu
			235		130
		Fibonacci (with 16.777.216 input elements)
			Gpu		Cpu
			470		226


--- CONCLUSIO & OPTIMIZATION ---

There is space for optimization in our implementation of the scan-primitive, since we are reading out buffers quite heavily. One possible enhancement could be the addition of partial sums as a GPU-kernel by simply passing the written sums-buffer instead of reading the buffer and passing it to a C#-method. This could possibly boost the primitive itself and therefore the applications in the compaction example.