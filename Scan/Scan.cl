﻿#define NUM_BANKS 32

__kernel void scan(__global int* input, __global int* output, __global int* workgroupSums, __local int* temp)
{
	int width = get_local_size(0) * 2;
	int thid = get_local_id(0);
	int group = get_group_id(0);

	int inputOffset = group * width;

	int offset = 1;

	int ai = 2 * thid;
	int bi = 2 * thid + 1;

	ai += ai / NUM_BANKS;
	bi += bi / NUM_BANKS;
	
	temp[ai] = input[inputOffset + 2 * thid];
	temp[bi] = input[inputOffset + 2 * thid + 1];

	//shifting to the right is equivalent to division by powers of 2.
	for (int d = width / 2; d > 0; d /= 2)// build sum in place up the tree
	{ 
		barrier(CLK_LOCAL_MEM_FENCE);
		if (thid < d)
		{
			int ai = offset*(2*thid+1)-1;
			int bi = offset*(2*thid+2)-1;

			ai += ai / NUM_BANKS;
			bi += bi / NUM_BANKS;

			temp[bi] += temp[ai];
		}
		offset *= 2;
	}

	if (thid==0) {
		workgroupSums[group] = temp[(width - 1) + (width/NUM_BANKS - 1)];
		temp[(width - 1) + (width/NUM_BANKS - 1)] = 0;
	}

	barrier(CLK_GLOBAL_MEM_FENCE);

	for (int d = 1; d < width; d *= 2) // traverse down tree & build scan
	{
		 offset /= 2;
		 barrier(CLK_LOCAL_MEM_FENCE);
		 if (thid < d)                     
		 {
			int ai = offset*(2*thid+1)-1;
			int bi = offset*(2*thid+2)-1;
			ai += ai / NUM_BANKS;
			bi += bi / NUM_BANKS;
			int t = temp[ai];
			temp[ai] = temp[bi];
			temp[bi] += t;
		}
	}
	barrier(CLK_LOCAL_MEM_FENCE);

	output[inputOffset + (2 * thid)] = temp[ai];
	output[inputOffset + (2 * thid + 1)] = temp[bi];
}