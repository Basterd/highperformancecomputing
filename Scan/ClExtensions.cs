﻿using System.Collections.Generic;
using OpenCL.Net;

namespace Scan
{
    public static class ClExtensions
    {
        public static void ReleaseMemObjects(this List<IMem> memObjects)
        {
            foreach (var memObject in memObjects)
            {
                Cl.ReleaseMemObject(memObject);
            }
        }
    }
}
