﻿using System;
using System.Linq;

namespace Scan
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var input = RandomInput(32_777_216);

            var scan = new Scan(input);

            var cpuResult = scan.ScanSequential();
            var gpuResult = scan.ScanGpu();
            Console.WriteLine();
            Console.WriteLine($"GPU-Result is {(gpuResult.SequenceEqual(cpuResult) ? "Right" : "Wrong")}!");
            Console.WriteLine("Compared runtime:");
            Console.WriteLine($"Seq ->\t{scan.SequentialTime}ms");
            Console.WriteLine($"GPU ->\t{scan.GpuTime}ms");
            var factor = (double)scan.SequentialTime / scan.GpuTime;
            Console.WriteLine($"GPU is Factor {factor} {(factor < 1 ? "slower" : "faster")}");

            Console.ReadLine();
        }

        private static int[] GetIntArray(string inputs) => inputs.Split(',').Select(int.Parse).ToArray();


        private static readonly Random Random = new Random();
        private static int[] RandomInput(int n)
        {
            var result = new int[n];
            for (var i = 0; i < n; i++)
            {
                result[i] = Random.Next(0, 50);
            }

            return result;
        }
    }
}
