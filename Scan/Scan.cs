﻿using OpenCL.Net;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Xml;

namespace Scan
{
    public class Scan
    {
        private int[] Inputs { get; }
        public long SequentialTime { get; private set; }
        public long GpuTime { get; private set; }

        private const int LocalWorkSize = 1024;
        private const int DoubleLocalWorkSize = LocalWorkSize * 2;

        public Scan(int[] inputs)
        {
            Inputs = inputs;
        }

        public int[] ScanSequential()
        {
            var watch = new Stopwatch();
            watch.Start();

            var result = new int[Inputs.Length];

            result[0] = Inputs[0];
            for (var i = 1; i < Inputs.Length; i++)
            {
                result[i] = result[i - 1] + Inputs[i];
            }

            watch.Stop();
            SequentialTime = watch.ElapsedMilliseconds;
            return new[] { 0 }.Concat(result.Take(result.Length - 1)).ToArray();
        }

        public int[] ScanGpu()
        {
            var platforms = Cl.GetPlatformIDs(out var _);

            var devices = Cl.GetDeviceIDs(platforms[1], DeviceType.All, out var _);

            var ctx = Cl.CreateContext(new[] { ContextProperty.Zero }, (uint)devices.Length, devices, null,
                IntPtr.Zero, out var _);

            var kernelSource = File.ReadAllText("Scan.cl");
            var prog = Cl.CreateProgramWithSource(ctx, 1, new[] { kernelSource }, new[] { new IntPtr(kernelSource.Length) }, out var error);

            error = Cl.BuildProgram(prog, 0, null, null, null, IntPtr.Zero);
            if (error != ErrorCode.Success)
            {
                var programInfo = Cl.GetProgramBuildInfo(prog, devices[0], ProgramBuildInfo.Log, out error);
                Console.WriteLine(programInfo.ToString());
                Console.ReadLine();
                return new[] { -1 };
            }

            var commandQueue = Cl.CreateCommandQueue(ctx, devices[0], CommandQueueProperties.None, out error);
            
            var sw = new Stopwatch();
            sw.Start();

            Tuple<int[], int[]> initialResult = DoScan(Inputs, ctx, commandQueue, prog);
            int[] scanResult = initialResult.Item1;

            if(initialResult.Item2.Length > 1)
                DoSumScanUntilWorkGoupSize(initialResult, ctx, commandQueue, prog, ref scanResult);

            var result = RemovePadding(scanResult, Inputs.Length);
            sw.Stop();
            GpuTime = sw.ElapsedMilliseconds;

            return result;
        }

        private static void DoSumScanUntilWorkGoupSize(Tuple<int[], int[]> initialResult, Context ctx, CommandQueue commandQueue,
            OpenCL.Net.Program prog, ref int[] scanResult)
        {
            var sumScans = new List<int[]>();
            var sumScanTuple = DoScan(initialResult.Item2, ctx, commandQueue, prog);
            sumScans.Add(RemovePadding(sumScanTuple.Item1, initialResult.Item2.Length));

            while (sumScans.Last().Length > DoubleLocalWorkSize)
            {
                int originalItemCount = sumScanTuple.Item2.Length;
                sumScanTuple = DoScan(sumScanTuple.Item2, ctx, commandQueue, prog);
                sumScans.Add(RemovePadding(sumScanTuple.Item1, originalItemCount));
            }

            int[] addedSumScans = GetAccumulatedSumScan(sumScans);
            for (int sumScanIndex = 0; sumScanIndex < addedSumScans.Length; sumScanIndex++)
            {
                for (var elementIndex = 0; elementIndex < DoubleLocalWorkSize; elementIndex++)
                {
                    scanResult[sumScanIndex * DoubleLocalWorkSize + elementIndex] += addedSumScans[sumScanIndex];
                }
            }
            
        }

        private static int[] GetAccumulatedSumScan(List<int[]> sumScans)
        {
            int[] accumSum = sumScans[0];

            for (int accumSumI = DoubleLocalWorkSize; accumSumI < accumSum.Length; accumSumI+= DoubleLocalWorkSize)
            {
                for (int i = 1; i < sumScans[1].Length; i++)
                {
                    for (int j = 0; j < DoubleLocalWorkSize; j++)
                    {
                        accumSum[accumSumI + j] += sumScans[1][i];
                    }

                    accumSumI += DoubleLocalWorkSize;
                }
            }

            return accumSum;
        }

        private static Tuple<int[], int[]> DoScan(int[] input, Context ctx, CommandQueue commandQueue, OpenCL.Net.Program prog)
        {
            var kernel = Cl.CreateKernel(prog, "scan", out var error);

            var paddedInputs = PadInputToPower2(input);
            var workgroupCount = GetWorkgroupCount(paddedInputs);

            var inputBuffer = Cl.CreateBuffer<int>(ctx, MemFlags.ReadOnly, paddedInputs.Length * sizeof(int), out error);
            error = Cl.EnqueueWriteBuffer(commandQueue, inputBuffer, Bool.True, paddedInputs, 0, null, out var _);
            error = Cl.SetKernelArg(kernel, 0, inputBuffer);

            var outputBuffer = Cl.CreateBuffer<int>(ctx, MemFlags.WriteOnly, paddedInputs.Length * sizeof(int), out error);
            error = Cl.SetKernelArg(kernel, 1, outputBuffer);

            var workgroupSumsBuffer = Cl.CreateBuffer<int>(ctx, MemFlags.WriteOnly, workgroupCount * sizeof(int), out error);
            error = Cl.SetKernelArg(kernel, 2, workgroupSumsBuffer);

            error = Cl.SetKernelArg(kernel, 3, new IntPtr(DoubleLocalWorkSize * sizeof(int)), null);
            
            error = Cl.EnqueueNDRangeKernel(commandQueue, kernel, 1, null, new[] { new IntPtr(paddedInputs.Length) }, new[] { new IntPtr(LocalWorkSize) }, 0, null, out var _);

            var result = new int[paddedInputs.Length];
            error = Cl.EnqueueReadBuffer(commandQueue, outputBuffer, Bool.True, 0, result.Length, result, 0, null, out var _);

            var workgroupSums = new int[workgroupCount];
            error = Cl.EnqueueReadBuffer(commandQueue, workgroupSumsBuffer, Bool.True, 0, workgroupCount, workgroupSums, 0, null, out var _);

            new List<IMem> { inputBuffer, outputBuffer, workgroupSumsBuffer }.ReleaseMemObjects();

            return Tuple.Create(result, workgroupSums);

        }

        private static int GetWorkgroupCount(IReadOnlyCollection<int> workItems)
        {
            return (int) Math.Ceiling((double) workItems.Count / (DoubleLocalWorkSize));
        }

        private static int[] RemovePadding(IReadOnlyList<int> result, int intialSize)
        {
            var unpaddedResult = new int[intialSize];
            for (var i = 0; i < intialSize; i++)
            {
                unpaddedResult[i] = result[i];
            }
            return unpaddedResult;
        }

        private static int[] PadInputToPower2(int[] input)
        {
            int oldPower = input.Length;
            int nextPower;
            
            do
            {
                nextPower = (int)Math.Round(Math.Pow(2, Math.Ceiling(Math.Log(oldPower, 2))));
                oldPower++;
            } while (nextPower % LocalWorkSize != 0);

            if (nextPower == input.Length)
                return input;

            var paddedInputs = new int[nextPower];
            for (var i = 0; i < input.Length; i++)
            {
                paddedInputs[i] = input[i];
            }

            return paddedInputs;
        }
    }
}
